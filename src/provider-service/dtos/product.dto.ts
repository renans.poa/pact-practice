import { OfferDto } from './offer.dto';

export class ProductDto {
    readonly adult_only: number;
    readonly brand: string;
    readonly cat: string[];
    readonly areaname_s: string;
    readonly group_info: {
        name: string;
        price_min: number;
        price_max: number;
    };
    readonly subfamilianame_id_s: string;
    readonly subfamilianame_s: string;
    readonly name: string;
    readonly images: any[];
    readonly is_hot_deal: number;
    readonly is_smart_deal: number;
    readonly productId: string;
    stores?: OfferDto[];
    readonly url;
    readonly priceMin: number;
    readonly priceMax: number;
    readonly rating: {
        value: number;
        count: number;
    };
    readonly tag: string;
    readonly top_50: string;
    readonly unique_product_id: number;
    readonly product_slug: string;
    readonly id: string;
}
