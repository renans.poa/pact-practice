export class OfferDto {
    store_id: string;
    cpc: number;
    cpa: number;
    product_name: string;
    shipping: string;
    store_tax: string;
    url: string;
    price_update: string;
    rating: number;
    is_cpc: boolean;
    is_marketplace: boolean;
    is_marketplace_algolia: boolean;
    import_auto: boolean;
    price: string;
    price_feed: string;
    old_price: number;
    old_price_feed: number;
    stock: number;
    stock_quantity: number;
    shipping_market: ShippingMarket;
    preparation_time: PreparationTime;
    service_points: string[];
    score: string;
    marketplace: number;
    force_top_box: boolean;
    sku_api: string;
}

interface ShippingMarket {
    normal: Shipping;
    express: Shipping;
}

export interface Shipping {
    value: number;
    delivery: {
        min: number;
        max: number;
    };
    preparation: {
        min: number;
        max: number;
    };
}

interface PreparationTime {
    min: ValueObj;
    max: ValueObj;
}

interface ValueObj {
    value: number;
}
