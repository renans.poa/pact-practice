import { Controller, Get, Param } from '@nestjs/common';
import { ProductDto } from './dtos/product.dto';
import { FilterService } from './filter.service';
import { ProductService } from './product.service';

@Controller('products')
export class ProductController {
    constructor(
        private readonly productService: ProductService,
        private readonly filterService: FilterService
    ) {}

    @Get(':id')
    async getProductById(@Param('id') productId: string): Promise<ProductDto> {
        return this.productService.getProductById(productId);
    }
}
