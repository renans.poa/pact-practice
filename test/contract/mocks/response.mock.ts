import { ProductDto } from '../../../src/provider-service/dtos/product.dto';

export const productDtoResponse: any = {
    productId: 'p-9-256477',
    name: 'iPhone 13 6.1" 256GB Green',
    priceMin: 858.9,
    url: '/9/256477',
    images: [
        {
            url_48: 'img_upload/produtos_comunicacoes/1155401_1.jpg',
            url_150: 'img_upload/produtos_comunicacoes/1155401_2.jpg',
            url_100: 'img_upload/produtos_comunicacoes/1155401_2b.jpg',
            url_600: 'img_upload/produtos_comunicacoes/1155401_3.jpg'
        }
    ],
    unique_product_id: 7759108
};

export const productWithoutStoresResponseDto: ProductDto = {
    adult_only: 0,
    brand: 'Pilopeptan',
    cat: ['Saude e Beleza'],
    areaname_s: 'Saude e Beleza',
    group_info: {
        name: 'Pilopeptan 60 Cápsulas',
        price_min: 23,
        price_max: 32.5
    },
    subfamilianame_id_s: '9-38',
    subfamilianame_s: '9-38-951544-r-Cabelo, Unhas e Pele',
    name: 'Pilopeptan 60 Cápsulas',
    images: [
        {
            url_48: 'img_upload/produtos_comunicacoes/1155401_3.jpg',
            url_100: 'img_upload/produtos_comunicacoes/1155401_1.jpg',
            url_150: 'img_upload/produtos_comunicacoes/1155401_2.jpg',
            url_600: 'img_upload/produtos_comunicacoes/1155401_2b.jpg'
        }
    ],
    is_hot_deal: 0,
    is_smart_deal: 0,
    productId: 'p-9-256477',
    tag: 'Pilopeptan-60-Capsulas',
    top_50: '0',
    unique_product_id: 7759108,
    product_slug: 'pilopeptan-60-capsulas',
    id: 'p-9-256477',
    url: '/9/256477',
    priceMin: 0,
    priceMax: 0,
    rating: {
        value: 0,
        count: 0
    }
};
