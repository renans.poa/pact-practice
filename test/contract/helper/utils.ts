import { MatchersV3 } from "@pact-foundation/pact";

const { eachLike, string, number, boolean, regex } = MatchersV3;

export function applyMatchersToDto(
  dto: Record<string, any>,
  regexMatchers?: Record<string, string>
): Record<string, MatchersV3.Matcher<any>> {
  const result: Record<string, MatchersV3.Matcher<any>> = {};

  for (const [key, value] of Object.entries(dto)) {
    if (regexMatchers && regexMatchers[key]) {
      result[key] = MatchersV3.regex(value, regexMatchers[key]);
    } else if (typeof value === "string") {
      result[key] = string(value);
    } else if (typeof value === "number") {
      result[key] = number(value);
    } else if (typeof value === "boolean") {
      result[key] = boolean(value);
    } else if (Array.isArray(value)) {
      result[key] = eachLike(applyMatchersToDto(value[0]));
    }
  }

  return result;
}
