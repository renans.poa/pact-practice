import { PactProviderModule, PactProviderOptions } from 'nestjs-pact';

export enum ConsumerNames {
    KK_SHOPPINGCART_SERVICE = 'KK-SHOPPINGCART-SERVICE',
    KK_PURCHASE_SERVICE = 'KK-PURCHASE-SERVICE',
    KK_WISHLIST_SERVICE = 'KK-WISHLIST-SERVICE',
    KK_CLICK_API = 'KK-CLICK-API',
    KK_SITEMAP_SERVICE = 'KK-SITEMAP-SERVICE',
    KK_APIGATEWAY = 'KK-APIGATEWAY'
}
const provider = 'KK-PRODUCT-SERVICE';

export const getPactProviderModule = (consumerFilters: ConsumerNames[]) => {
    const options: PactProviderOptions = {
        provider: provider,
        providerVersion: '1.0.0',
        pactBrokerUrl: 'https://95c1-85-139-254-113.ngrok-free.app/',
        logLevel: 'debug',
        enablePending: true,
        consumerVersionSelectors: consumerFilters.map(consumer => ({
            tag: 'develop',
            latest: true,
            consumer: consumer
        })),
        publishVerificationResult: true
    };

    return PactProviderModule.register(options);
};
