import { CacheModule, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { PactVerifierService } from 'nestjs-pact';
import { ConsumerNames, getPactProviderModule } from '../helper/pact.module';
import { ProductController } from 'src/provider-service/product.controller';
import { ProductService } from 'src/provider-service/product.service';
import { FilterService } from 'src/provider-service/filter.service';
import { productWithoutStoresResponseDto } from '../mocks/response.mock';
jest.mock('src/provider-service/filter.service');

describe('Pact Verification', () => {
    let verifierService: PactVerifierService;
    let app: INestApplication;
    let module: TestingModule;

    beforeAll(async () => {
        module = await Test.createTestingModule({
            imports: [
                CacheModule.register(),
                getPactProviderModule([ConsumerNames.KK_WISHLIST_SERVICE])
            ],
            controllers: [ProductController],
            providers: [
                FilterService,
                {
                    provide: ProductService,
                    useValue: {
                        getProductById: jest
                            .fn()
                            .mockResolvedValue([
                                productWithoutStoresResponseDto
                            ])
                    }
                }
            ]
        }).compile();

        verifierService = module.get(PactVerifierService);
        app = module.createNestApplication();
    });

    describe('Contract Test', () => {
        it('Should verify the pact contracts successfully', async () => {
            await verifierService.verify(app);
        });
    });

    afterAll(async () => {
        jest.resetAllMocks();
        jest.resetModules();
        jest.clearAllMocks();
        await app.close();
    });
});
