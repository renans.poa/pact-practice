import { PactV3 } from '@pact-foundation/pact';
import * as path from 'path';
import axios from 'axios';
import * as dotenv from 'dotenv';
import { applyMatchersToDto } from '../helper/utils';
import { productDtoResponse } from '../mocks/response.mock';

dotenv.config({ path: './.env.test' });

const apiBaseUrl = process.env.BASE_URL_AXIOS_REQUEST;
const ACCEPT_HEADER = { 'Content-Type': 'application/json' };
const PRODUCT_WITHOUT_STORES_PATH = '/products/without-stores';
const PRODUCT_IDS_QUERY = 'p-3-533094';

const pact = new PactV3({
    consumer: 'KK-WISHLIST-SERVICE',
    provider: 'KK-PRODUCT-SERVICE',
    port: 1234,
    dir: path.resolve(process.cwd(), './test/contract/pacts'),
    logLevel: 'debug'
});

const regexMatchers = {
    productId: '^p-\\d-[0-9]+$',
    url: '^/\\d/[0-9]+$'
};

const mockData = applyMatchersToDto(productDtoResponse, regexMatchers);

describe('Consumer Contract Test', () => {
    describe('When a GET request is made to retrieve product information', () => {
        it('returns the correct product data', async () => {
            pact.given(
                `GET - ${PRODUCT_WITHOUT_STORES_PATH} - Eligible payment methods exist for the provided checkout items`
            )
                .uponReceiving(
                    'A request to get eligible payment methods for the provided checkout items'
                )
                .withRequest({
                    method: 'GET',
                    path: PRODUCT_WITHOUT_STORES_PATH,
                    query: {
                        productIds: PRODUCT_IDS_QUERY
                    },
                    headers: ACCEPT_HEADER
                })
                .willRespondWith({
                    status: 200,
                    headers: ACCEPT_HEADER,
                    body: [mockData]
                });

            return pact.executeTest(async () => {
                const response = await axios.get(
                    `${apiBaseUrl}${PRODUCT_WITHOUT_STORES_PATH}`,
                    {
                        headers: ACCEPT_HEADER,
                        params: {
                            productIds: PRODUCT_IDS_QUERY
                        }
                    }
                );
                const product = response.data[0];
                expect(typeof product.priceMin).toBe('number');
                expect(typeof product.productId).toBe('string');
            });
        });
    });
});
